/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saharat.helloworld;

/**
 *
 * @author Dell
 */

import java.util.Scanner;

public class OX {

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showBoard(String[][] board) { 
        System.out.println(" 1 2 3");
        int p = 1;
        for (int i = 0; i < board.length; i++) {
            System.out.print(p++);
            for (int j = 0; j < board.length; j++) {
                System.out.print(board[i][j] + " ");               
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        String[][] board = new String[][]{
        {"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"}};

        showWelcome();
        showBoard(board);
    }

}
